import React, {createRef} from 'react';
import {Input} from "reactstrap";
import "./App.css";

const colors = ["black", "red", "blue", "yellow", "gray"];

class App extends React.Component {
    state = {
        color: 'black',
        circles: []
    };
    canvas = createRef();

    componentDidMount() {
        const canvas = this.canvas.current;
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        this.websocket = new WebSocket('ws://localhost:8000/images');

        this.websocket.onmessage = (message) => {
            try {
                const data = JSON.parse(message.data);

                if (data.type === 'NEW_CIRCLE') {
                    const newCircle = {
                        color: data.color,
                        x: data.x,
                        y: data.y
                    };
                    this.circlePrint(newCircle);
                    this.setState({circles: [...this.state.circles, newCircle]})

                } else if (data.type === 'ALL_CIRCLES') {
                    data.circles.forEach(circle => {
                        this.circlePrint(circle);
                    });
                    this.setState({circles: data.circles});
                }
            } catch (e) {
                console.log('Something went wrong', e);
            }
        };

    }

    circlePrint = (circle) => {
        const canvas = this.canvas.current;
        const ctx = canvas.getContext('2d');
        const rect = canvas.getBoundingClientRect();
        const x = circle.x - rect.left;
        const y = circle.y - rect.top;
        ctx.beginPath();
        ctx.fillStyle = circle.color;
        ctx.arc(x, y, 20, 0, 2 * Math.PI, true);
        ctx.fill();
    };

    onCanvasClick = e => {
        e.persist();
        const circle = {
            type: 'CREATE_CIRCLE',
            color: this.state.color,
            x: e.clientX,
            y: e.clientY
        };
        this.websocket.send(JSON.stringify(circle));
    };
    changeField = e => this.setState({[e.target.name]: e.target.value});

    render() {
        return (
            <>
                <Input type="select" name="color" value={this.state.color} onChange={this.changeField}>
                    {colors.map(c => (
                        <option key={c} value={c}>
                            {c}
                        </option>
                    ))}
                </Input>
                <canvas ref={this.canvas} onClick={this.onCanvasClick}/>
            </>
        );
    }
}

export default App;




