const express = require('express');
const cors = require('cors');
const expressWs = require('express-ws');
const {nanoid} = require('nanoid');

const app = express();
const port = 8000;

expressWs(app);

app.use(express.json());
app.use(cors());

const connections = {};

const circles = [];

app.ws('/images', function (ws, req) {
    const id = nanoid();
    console.log(`client connected id=' + ${id}`);

    connections[id] = ws;


    ws.send(JSON.stringify({
        type: 'ALL_CIRCLES',
        circles: circles
    }));

    ws.on('message', (crl) => {
        const parsed = JSON.parse(crl);

        switch (parsed.type) {
            case 'CREATE_CIRCLE':
                Object.keys(connections).forEach(connId => {
                    const connection = connections[connId];
                    const newCircle = {
                        x: parsed.x,
                        y: parsed.y,
                        color: parsed.color
                    };

                    connection.send(JSON.stringify({
                        type: 'NEW_CIRCLE',
                        ...newCircle
                    }));

                    circles.push(newCircle);
                });
                break;
            default:
                console.log('NO TYPE: ' + parsed.type);
        }
    });

    ws.on('close', (crl) => {
        console.log(`client disconnected! ${id}`);
        delete connections[id];
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});

